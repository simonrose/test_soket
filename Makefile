TGTS = socket_client socket_server ifl

.PHONY: build
build: $(TGTS)

$(foreach tgt,$(TGTS),$(eval $(tgt): $(tgt).o))

clean: 
	rm -f $(TGTS:=.o)